import 'package:flutter/material.dart';

class IndexPage extends StatelessWidget {
  const IndexPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('내사랑 팡'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
                'assets/pan.jpg',
              width: 500,
              height: 250,
            ),
            Container(
              child: Column(
                children: [
                  Text('팡이를 소개합니다'),
                  Text('팡이는 귀여워요. 하지만 귀찮아요'),
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset(
                          'assets/pan.jpg',
                          width: 250,
                        height: 125,
                      ),
                      Image.asset(
                          'assets/pan.jpg',
                          width: 250,
                        height: 125,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset(
                          'assets/pan.jpg',
                        width: 250,
                        height: 125,
                      ),
                      Image.asset(
                          'assets/pan.jpg',
                        width: 250,
                        height: 125,
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
